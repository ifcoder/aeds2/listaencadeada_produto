#include "Nodo.h"

Nodo::Nodo() {
    prox = NULL;    
}

Nodo::Nodo(Produto& p){
    item.copiar(p);
    prox = NULL;
}


Nodo::Nodo(const Nodo& outro) {
    item.copiar(outro.getItem());
}

Nodo::~Nodo() {
}

/**
 * GETTERS e SETTERS
 */

void Nodo::setProx(Nodo* prox) {
    this->prox = prox;
}

Nodo* Nodo::getProx() const {
    return prox;
}

void Nodo::setItem(Produto item) {
    this->item = item;
}

Produto Nodo::getItem() const {
    return item;
}

