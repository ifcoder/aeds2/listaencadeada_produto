# Lista Encadeada (Produto)

- Este código C++ pertence a disciplina Algoritmos e Estrutura de Dados II
- Esta é a segunda estrutura de dados que vamos estudar
- [Material de estudo](https://summer-pocket-6a4.notion.site/2-2-Lista-Encadeada-7d08379a41d247118aea47df85516ad7) 

## Lista Encadeada
- A lista encadeada parece com uma corrente, ou seja sua formação básica são os nós(NODOS)
[] Chamo bastante atenção para os casos de inserção e remoção na primeira e na última posição
[] Estes casos devem ser tratados com bastante atenção
  
## Linguagem C++
- O estudo desta disciplina é feito usando a linguagem de programação C++

## NetBeans
- Este projeto foi desenvolvido utilizando a IDE Netbeans. 
- Para rodá-lo é necessário ter o compilador C++ em sua máquina e a IDE NetBeans configurada para a linguagem C++

